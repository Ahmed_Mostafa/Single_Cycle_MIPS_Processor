module processor_JAL ;	   
	wire clk;
	clock Mclk(clk);
	wire [31:0] in_pc,out_pc;
	PC Mpc(out_pc,clk,in_pc);
	///////////////////////////////
	wire [31:0]instruction;
	instruction_memory mim(instruction,out_pc);
	//////////////////////////////]
	wire Jr; //From Alu Control unit
	wire Jump; //From Control Unit
	wire RegWrite_CU,RegWrite_RF; //From Main control unit
	wire [1:0]RegDst;
	wire [5:0]opCode,funct;
	wire [4:0]in_rr1,in_rr2,in_wr,in_mux;
	wire [15:0]immediate;
	wire [31:0] in_write_data;//gy mn el Memory ll Reg file	 
	wire [31:0] out_rd1,out_rd2;
	
	assign opCode=instruction[31:26];
	assign funct=instruction[5:0];
	assign in_rr1=instruction[25:21];
	assign in_rr2=instruction[20:16];
	assign in_mux=instruction[15:11];
	assign immediate=instruction[15:0];	
	
	wire not_Jr;
	not #5 not_Jr_gate(not_Jr,Jr);
	and #5 and_RegWrite(RegWrite_RF,RegWrite_CU,not_Jr);
	
	wire [32*32-1:0] allRegisters;//used to print values
	mux_4x1 Mrfmux(in_wr , in_rr2 , in_mux , 5'd31 , 5'bzzzzz ,RegDst); 
	registerFile Mrf(allRegisters,out_rd1,out_rd2,in_rr1,in_rr2,in_wr,in_write_data,RegWrite_RF,clk);
	
	//////////////////////////
	wire SignedExtend,Branch,MemRead,MemWrite,ALUSrc;
	wire [1:0]ALUOp,MemtoReg;
	control_unit_JAL Mcu(Jump,SignedExtend,RegDst,ALUSrc,MemtoReg,RegWrite_CU,MemRead,MemWrite,Branch,ALUOp,opCode);	 
	//////////////////////////////	
	
	wire [31:0]SignedExtendedImmediate,UnsignedExtendedImmediate;
	signExtend Mse(SignedExtendedImmediate,immediate);  
	unsignedExtend Muse(UnsignedExtendedImmediate,immediate);
	wire [31:0]ExtendedImmediate;
	mux_2x1 immediate_mux(ExtendedImmediate ,UnsignedExtendedImmediate,SignedExtendedImmediate,SignedExtend);
	//////////////// 
	wire [3:0]ALU_ctrl;
	aluControl MaluCtrl(ALU_ctrl,Jr,ALUOp,funct);
	/////////////////				 
	
	wire [31:0]alu_in2;
	mux_2x1 alumux(alu_in2 ,out_rd2,ExtendedImmediate,ALUSrc);
	wire [4:0]shamt;
	wire [31:0]ALU_res;
	wire Zero;
	assign shamt=instruction[10:6];
	aLu Malu(ALU_res , Zero , out_rd1 ,alu_in2 ,shamt ,ALU_ctrl);
	////////////////////////////////////
	wire [31:0]out_add;
	Adder Madd(out_add,out_pc,4);
	//////////////////////////////////
	wire [31:0] out_data_mem;
	dataMemory Mdm(out_data_mem,out_rd2,ALU_res,MemRead,MemWrite);	
	mux_4x1 datamemmux(in_write_data,ALU_res,out_data_mem,out_add,32'bzz,MemtoReg);
	////////////////////////////////////   
	wire [31:0]out_sll;
	sll_2 Msll(out_sll,SignedExtendedImmediate);
	 
	////////////////////////////
	wire [31:0]out_adder;
	Adder Madd2(out_adder,out_add,out_sll); 
	///////////////////////////////////
	
	wire out_and;
	and #10 andgate(out_and,Branch,Zero);
	wire[31:0] pc_BeqAndPcPlus4;
	mux_2x1 pcmux_beq_pc4(pc_BeqAndPcPlus4 ,out_add,out_adder,out_and);	
	
	////// jal
	wire [31:0]in_pc_mux;
	mux_2x1 jalmux(in_pc_mux ,pc_BeqAndPcPlus4,{out_add[31:28],instruction[25:0],2'b00},Jump);
	//mux_2x1 jalmux(in_pc_mux ,pc_BeqAndPcPlus4,32/b1,Jump);
	////////////////////////////////JR part
	
	mux_2x1 pcmux(in_pc ,in_pc_mux,out_rd1,Jr);
	
	reg [0:0] terminate;
	
	valuesExport MipsValueExport(clk,allRegisters,terminate);	
	
	 
	initial begin
		#90000 terminate=1;
		//#5000 terminate=1;
		$finish;
	end
endmodule