module valuesExport(clk,allRegisters,terminate,);
input clk,terminate;
reg [31:0]register_file[0:31];
integer f,i,indexOfClockCycle;

input [32*32-1:0] allRegisters;

always @* begin
  for (i = 0; i < 32; i = i+1)
    register_file[i] <= allRegisters[32*i +: 32];
end


initial begin
  f <= $fopen("output.txt","w");
  indexOfClockCycle<=1;
end

always @(posedge clk)
begin 
$fwrite(f,"Clock Cycle No :%d\n",indexOfClockCycle);
for (i = 0; i<32; i=i+1)
$fwrite(f,"Register%d: %d\n",i,register_file[i]);
$fwrite(f,"*****************************************************************************\n");
indexOfClockCycle=indexOfClockCycle+1;
end


always @(terminate)
  $fclose(f);  

endmodule
