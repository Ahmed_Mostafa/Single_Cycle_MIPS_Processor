module aLu(ALU_result , Zero , Read_data1 ,Read_data2 ,shamt ,ALU_ctrl);
	input [31:0] Read_data1 ,  Read_data2 ;
	input [3:0] ALU_ctrl ;
	input [4:0] shamt ;
	reg [31:0] temp;
	output [31:0] ALU_result;
	output Zero ;
	assign #90 Zero = (ALU_result==0); 
	assign #90 ALU_result=temp;
  always@(ALU_ctrl , Read_data1 , Read_data2)begin
	  case(ALU_ctrl)	 
		     	4'b0000: temp <= Read_data1 & Read_data2; //and
			4'b0001: temp <= Read_data1 | Read_data2; //or 
			4'b0010: temp <= Read_data1 + Read_data2 ;	//add	
			4'b0011: temp <= Read_data1 << shamt ; // sll - Added by bogy
			4'b0110: temp <= Read_data1 - Read_data2 ;	//sub
			4'b0111: temp <= Read_data1 < Read_data2 ? 1:0 ;//slt	
			4'b1100: temp <= ~(Read_data1 | Read_data2);//nor
			4'b1111: temp <=32'bx;//NOP - used in jr
			default: temp = 32'bx;
		endcase
		
  end
endmodule			