module registerFile (allRegisters,readData1,readData2,readRegister1,readRegister2,writeRegister,writeData,RegWrite,clk);
	input [4:0] readRegister1;
	input [4:0] readRegister2;
	input [4:0] writeRegister;
	input [31:0] writeData;
	input RegWrite;
	input clk ;
	output [31:0]readData1;
	output [31:0]readData2;
	reg [31:0] register_file [0:31];
    	wire [31:0]temp;
	assign #90 readData1=register_file[readRegister1];
	assign #90 readData2=register_file[readRegister2];
	initial
		register_file[0]=0;



	assign #90 temp=(RegWrite)?writeData:register_file[writeRegister];
	always @(posedge clk) begin
		register_file[writeRegister] = temp;
	end
	


	output [32*32-1:0] allRegisters;

  	genvar i;
  	generate for (i = 0; i < 32; i = i+1) begin:instmem
   	 assign allRegisters[32*i +: 32] = register_file[i]; 
  	end endgenerate
endmodule