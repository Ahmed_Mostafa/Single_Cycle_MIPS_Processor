module aLu(ALU_result , Zero , Read_data1 ,Read_data2 ,shamt ,ALU_ctrl);
	input [31:0] Read_data1 ,  Read_data2 ;
	input [3:0] ALU_ctrl ;
	input [4:0] shamt ;
	reg [31:0] temp;
	output [31:0] ALU_result;
	output Zero ;
	assign #90 Zero = (ALU_result==0); 
	assign #90 ALU_result=temp;
  always@(ALU_ctrl , Read_data1 , Read_data2)begin
	  case(ALU_ctrl)	 
		     
			2: temp <= Read_data1 + Read_data2 ;	//add		
			6: temp <= Read_data1 - Read_data2 ;	//sub
			1: temp <= Read_data1 | Read_data2; //or 
			0: temp <= Read_data1 & Read_data2; //and
			12: temp <= ~(Read_data1 | Read_data2);//nor
			7: temp <= Read_data1 < Read_data2 ? 1:0 ;//slt   
			3: temp <= Read_data1 << shamt ; // additional
			default: temp = 32'bx;
		endcase
		
  end
endmodule			