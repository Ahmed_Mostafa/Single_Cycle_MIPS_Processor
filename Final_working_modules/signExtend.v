module signExtend(const_32bit,const_16bit);
input [15:0]const_16bit;
output [31:0] const_32bit;
assign #15 const_32bit={ {16{const_16bit[15]}} ,const_16bit};
endmodule