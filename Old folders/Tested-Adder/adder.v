module Adder ( A , B , OutAdder );
input [31:0] A, B ;
output OutAdder ;
reg [31:0] OutAdder;
always @( A , B)
begin
#70 OutAdder <= A+B ;
end	

endmodule
