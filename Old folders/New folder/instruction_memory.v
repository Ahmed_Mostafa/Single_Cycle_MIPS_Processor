module instruction_memory(current_inst,mem_address);
	input [31:0] mem_address;
	output[31:0] current_inst;
	wire [31:0] current_inst;
	reg [31:0]temp;
	reg [31:0] inst_memory [39:0]; //read Eng Diaa Mohamed comment https://www.facebook.com/groups/cse.asu.2017/permalink/1658313694409186/ if u need
	assign #200 current_inst=temp;
	initial begin
	  $readmemh("inst_mem.h",inst_memory);  
	end
	
	always @(mem_address) begin
	  temp <= inst_memory[mem_address];
	end

endmodule
  
