module processor ;	   
	wire clk;
	clock Mclk(clk);
	wire [31:0] in_pc,out_pc;
	PC Mpc(out_pc,clk,in_pc);
	///////////////////////////////
	wire [31:0]instruction;
	instruction_memory mim(instruction,out_pc);
	//////////////////////////////
	wire RegDst,RegWrite; //From control unit
	wire [5:0]opCode,funct;
	wire [4:0]in_rr1,in_rr2,in_wr,in_mux;
	wire [15:0]immediate;
	wire [31:0] in_write_data;//gy mn el Memory ll Reg file	 
	wire [31:0] out_rd1,out_rd2;
	
	assign opCode=instruction[31:26];
	assign funct=instruction[5:0];
	assign in_rr1=instruction[25:21];
	assign in_rr2=instruction[20:16];
	assign in_mux=instruction[15:11];
	assign immediate=instruction[15:0];	
	
	
	mux_2x1 Mrfmux(in_wr , in_rr2 , in_mux ,RegDst); 
	registerFile Mrf(out_rd1,out_rd2,in_rr1,in_rr2,in_wr,in_write_data,RegWrite);
	
	//////////////////////////
	wire Branch,MemRead,MemtoReg,MemWrite,ALUSrc;
	wire [1:0]ALUOp;
	control_unit Mcu(RegDst,ALUSrc,MemtoReg,RegWrite,MemRead,MemWrite,Branch,ALUOp,opCode);	 
	//////////////////////////////	
	
	wire [31:0]se_mux;
	signExtend Mse(se_mux,immediate);  
	//////////////// 
	wire [3:0]ALU_ctrl;
	aluControl MaluCtrl(ALU_ctrl,ALUOp,funct);
	/////////////////				 
	
	wire [31:0]alu_in2;
	mux_2x1 alumux(alu_in2 ,out_rd2,se_mux,ALUSrc);
	wire [4:0]shamt;
	wire [31:0]ALU_res;
	wire Zero;
	assign shamt=instruction[10:6];
	aLu Malu(ALU_res , Zero , out_rd1 ,alu_in2 ,shamt ,ALU_ctrl);
	//////////////////////////////////
	wire [31:0] out_data_mem;
	
	dataMemory Mdm(out_data_mem,out_rd2,ALU_res,MemRead,MemWrite);	
	mux_2x1 datamemmux(in_write_data,ALU_res,out_data_mem,MemtoReg);
	////////////////////////////////////   
	 wire [31:0]out_add;
	 Adder Madd(out_add,out_pc,4);
	////////////////////////////////////
	wire [31:0]out_sll;
	sll_2 Msll(out_sll,se_mux);
	///////////////////////////////////
	 wire [31:0]out_adder;
	 Adder Madd2(out_adder,out_add,out_sll);   
	 ////////////////////////////
	 wire out_and;
	 and #10 andgate(out_and,Branch,Zero);
	 mux_2x1 pcmux(in_pc ,out_add,out_adder,out_and);
	 
	initial 
		#7000 $finish;
	
endmodule