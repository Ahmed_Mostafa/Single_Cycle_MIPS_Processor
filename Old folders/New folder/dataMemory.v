module dataMemory(dataOut,dataIn,address,MemRead,MemWrite);
output[31:0]dataOut;
wire[31:0]temp;

input [31:0]dataIn,address;
input MemRead,MemWrite;

reg [31:0]memory[0:511];

initial begin
	$readmemh("memory.h",memory);
end

assign #250 dataOut=(MemRead && !MemWrite)?memory[address]:32'hx;
assign #250 temp=(!MemRead && MemWrite)?dataIn:temp;
always @(temp)begin
memory[address]<=temp;
end
endmodule                              