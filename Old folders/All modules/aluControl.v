module aluControl(op,ALUOP,funct);
	reg [3:0] temp;
	output [3:0]op; 
	input [3:0]ALUOP;
	input [5:0]funct;
	//This module was assigned a delay of 50  unit
	assign #90 op=temp;
	always @(ALUOP,funct)
		begin
			case(ALUOP) 
			0:temp=4'b0010;//add
			1:temp=4'b0110;//sub
			2:
				case(funct)
				0:temp=4'b0011;//sll --Added by Bogy
				32:temp=4'b0010;//add
				36:temp=4'b0000;//and
				39:temp=4'b1100;//nor
				42:temp=4'b0111;//slt
				default:temp=4'bxxxx;
				endcase	
			3:temp=4'b0000;//andi -- Added by Amr (Not in MIPS datapath)
			default:temp=4'bxxxx;
			endcase
		end
endmodule