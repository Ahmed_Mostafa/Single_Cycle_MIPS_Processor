module registerFile (readData1,readData2,readRegister1,readRegister2,writeRegister,writeData,RegWrite);
	input [4:0] readRegister1;
	input [4:0] readRegister2;
	input [4:0] writeRegister;
	input [31:0] writeData;
	input RegWrite;
	output [31:0]readData1;
	output [31:0]readData2;
	reg [31:0] register_file [31:0];
    	wire [31:0]temp;
	assign #90 readData1=register_file[readRegister1];
	assign #90 readData2=register_file[readRegister2];
	initial
		register_file[0]=0;



	assign #90 temp=(RegWrite)?writeData:register_file[writeRegister];
	always @(temp) begin
		register_file[writeRegister] = temp;
	end
	
endmodule