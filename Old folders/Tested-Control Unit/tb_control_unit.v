module tb_control_unit;		  
	reg [5:0] opCode;
	wire RegDst,ALUSrc,MemtoReg,RegWrite,MemRead,MemWrite,Branch;
	wire [1:0] ALUOp;
    control_unit test_control_unit (RegDst,ALUSrc,MemtoReg,RegWrite,MemRead,MemWrite,Branch,ALUOp,opCode);
	
	initial begin
		#10 opCode=0;
	end
			
endmodule
