module control_unit(RegDst,ALUSrc,MemtoReg,RegWrite,MemRead,MemWrite,Branch,ALUOp,opCode);
	input [5:0] opCode;
	output RegDst,ALUSrc,MemtoReg,RegWrite,MemRead,MemWrite,Branch;
	output [1:0] ALUOp;	//currently 2 bits will be changed to 4 when needed
	
	
	assign RegDst =  (opCode == 6'b000000) ? 1: 
	                 (opCode == 6'b100011) ? 0:
					 (opCode == 6'b101011) ? 1'bx:
					 (opCode == 6'b000100) ? 1'bx: 1'bx;
	
    assign ALUSrc =  (opCode == 6'b000000) ? 0: 
	                 (opCode == 6'b100011) ? 1:
					 (opCode == 6'b101011) ? 1:
					 (opCode == 6'b000100) ? 0: 1'bx;
	
    assign MemtoReg =(opCode == 6'b000000) ? 0: 
	                 (opCode == 6'b100011) ? 1:
					 (opCode == 6'b101011) ? 1'bx:
					 (opCode == 6'b000100) ? 1'bx: 0; 
					
	assign RegWrite =(opCode == 6'b000000) ? 1: 
	                 (opCode == 6'b100011) ? 1:
					 (opCode == 6'b101011) ? 0:
					 (opCode == 6'b000100) ? 0: 0;
	
	assign MemRead =(opCode == 6'b000000) ? 0: 
	                (opCode == 6'b100011) ? 1:
					(opCode == 6'b101011) ? 0:
					(opCode == 6'b000100) ? 0: 0;
	
	assign MemWrite=(opCode == 6'b000000) ? 0: 
	                (opCode == 6'b100011) ? 0:
					(opCode == 6'b101011) ? 1:
					(opCode == 6'b000100) ? 0: 0;
					
	assign Branch = (opCode == 6'b000000) ? 0: 
	                (opCode == 6'b100011) ? 0:
					(opCode == 6'b101011) ? 0:
					(opCode == 6'b000100) ? 1: 0;
					 
	assign ALUOp =  (opCode == 6'b000000) ? 01: 
	                (opCode == 6'b100011) ? 00:
					(opCode == 6'b101011) ? 00:
					(opCode == 6'b000100) ? 00: 2'bxx;				 
		
				
	
endmodule