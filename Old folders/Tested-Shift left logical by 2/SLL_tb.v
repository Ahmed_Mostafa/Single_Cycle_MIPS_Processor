module SLL_tb;
reg [31:0]in;
wire [31:0]out;
sll_2 s(out,in);
initial begin
in=32'b1;
#5 in=32'b10;
#15 in=32'b1;
end
endmodule