module sll_2(out,in);
	output [31:0] out;
	input  [31:0] in ;	
	assign #10 out = in << 2; 	
endmodule
