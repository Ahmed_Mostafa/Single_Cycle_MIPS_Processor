module mux_tb;
	reg [31:0] A,B;
	wire [31:0] C;	
	reg [0:0] D ;
	mux_2x1 mu(C,A,B,D);
	initial
		begin
			A= 10 ;
			B = 20;
			D=0;
			#30 D=1;
			#10 D=0;
			#10 D=1;
			#20 D=0;
		end
	endmodule
	
	