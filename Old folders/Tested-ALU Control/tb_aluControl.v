module tb_aluControl;
	wire [3:0] op;
	reg [3:0]ALUOP;
	reg [5:0]funct;
	aluControl testAluControl(op,ALUOP,funct);
	
	initial begin
		#10 ALUOP = 0;
		#10 funct = 0;
	end
endmodule
