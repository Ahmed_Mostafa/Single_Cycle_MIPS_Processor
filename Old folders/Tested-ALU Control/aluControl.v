module aluControl(op,ALUOP,funct);
	output reg [3:0] op;
	input [3:0]ALUOP;
	input [5:0]funct;
	//This module was assigned a delay of 50  unit
	
	always @(ALUOP,funct)
		begin
			case(ALUOP) 
			0:op=4'b0010;//add
			1:op=4'b0110;//sub
			2:
				case(funct)
				0:op=4'b0011;//sll --Added by Bogy
				32:op=4'b0010;//add
				36:op=4'b0000;//and
				39:op=4'b1100;//nor
				42:op=4'b0111;//slt
				default:op=4'bxxxx;
				endcase	
			3:op=4'b0000;//andi -- Added by Amr (Not in MIPS datapath)
			default:op=4'bxxxx;
			endcase
		end
endmodule