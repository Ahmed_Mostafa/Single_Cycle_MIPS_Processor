module tb();
wire[31:0]dataOut;

reg [31:0]dataIn,address;
reg MemRead,MemWrite;
dataMemory dm(dataOut,dataIn,address,MemRead,MemWrite);
initial begin
dataIn<=32'hF;
address<=32'hF;
MemWrite<=1;
MemRead<=0;

#5 MemWrite<=0;
MemRead<=1;
#5 MemWrite<=0;
MemRead<=0;
#10 $finish;
end
endmodule