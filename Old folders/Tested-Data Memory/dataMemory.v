module dataMemory(dataOut,dataIn,address,MemRead,MemWrite);
output[31:0]dataOut;
reg[31:0]dataOut;

input [31:0]dataIn,address;
input MemRead,MemWrite;

reg [31:0]memory[0:511];

initial begin
$readmemh("memory.h",memory);
end

always @(dataIn,address,MemRead,MemWrite)begin
if(MemRead==1'b1)begin
#250 dataOut<=memory[address];
end
else if (MemWrite==1'b1)begin
#250 memory[address]<=dataIn;
dataOut<=32'hz;
end
else
dataOut<=32'hz;
end
endmodule                              