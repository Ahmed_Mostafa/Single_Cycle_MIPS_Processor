module signExtend_tb;
reg [15:0] inbits;
initial begin
inbits=16'b1000000011110101;
#10 inbits=16'b0000000011110101;
#15 inbits=16'b1000000011110101;
end
wire [31:0]outbits;
signExtend se(outbits,inbits);

endmodule
