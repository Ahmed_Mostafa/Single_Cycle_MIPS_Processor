module registerFile_tb;
	reg [4:0] readRegister1;
	reg [4:0] readRegister2;
	reg [4:0] writeRegister;
	reg [31:0] writeData;
	reg RegWrite;
	wire readData1;
	wire readData2;
	
	registerFile testReg (readData1,readData2,readRegister1,readRegister2,writeRegister,writeData,RegWrite);
	
	initial begin
		/////writing some data to register number 0 and 1
		#5  RegWrite=0;
		#5  RegWrite=1;
	    #20 writeRegister=5'b00000;
		#20 writeData=8'hAA;
		#20 writeRegister=5'b00001;
		#20 writeData=8'hAB;		
		/////////////////// Reading what was written
		#20 RegWrite=0;
		#20 readRegister1=5'b00000;
		#20 readRegister2=5'b00001;
		#100 $finish;
		
	end
	
	
	
	
	
	
endmodule

	