module registerFile (readData1,readData2,readRegister1,readRegister2,writeRegister,writeData,RegWrite);
	input [4:0] readRegister1;
	input [4:0] readRegister2;
	input [4:0] writeRegister;
	input [31:0] writeData;
	input RegWrite;
	output readData1;
	output readData2;
	reg [31:0] register_file [31:0];
    
	assign readData1=register_file[readRegister1];
	assign readData2=register_file[readRegister2];
	
	always @(RegWrite or writeData or writeRegister) begin
		if(RegWrite) begin
		register_file[writeRegister] = writeData ;
		end
	end
	
endmodule