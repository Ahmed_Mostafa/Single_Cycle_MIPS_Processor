module aLu(ALU_result , Zero , Read_data1 ,Read_data2 ,shamt ,ALU_ctrl);
	input [31:0] Read_data1 ,  Read_data2 ;
	input [3:0] ALU_ctrl ;
	input [4:0] shamt ;
	output reg [31:0] ALU_result;
	output Zero ;
	assign Zero = (ALU_result==0); 
  always@(ALU_ctrl , Read_data1 , Read_data2)begin 
	  #90;
	  case(ALU_ctrl)	 
		     
			2: ALU_result <= Read_data1 + Read_data2 ;	//add		
			6: ALU_result <= Read_data1 - Read_data2 ;	//sub
			1: ALU_result <= Read_data1 | Read_data2; //or 
			0: ALU_result <= Read_data1 & Read_data2; //and
			12: ALU_result <= ~(Read_data1 | Read_data2);//nor
			7: ALU_result <= Read_data1 < Read_data2 ? 1:0 ;//slt   
			3: ALU_result <= Read_data1 << shamt ; // additional
			default: ALU_result = 32'bx;
		endcase
		
  end
endmodule			