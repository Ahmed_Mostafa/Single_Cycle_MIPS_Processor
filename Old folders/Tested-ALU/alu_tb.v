module aLu_tb ;
	reg[31:0] A , B ;  
	reg[3:0] C;	 
	reg[4:0] E ;
	wire [31:0] D; 
	wire F;
	aLu l(D ,F,A,B,E,C);
	initial begin
		A = 1;
		B = 2;
		E = 2;
		C= 2;
		#100 $finish;
	end
endmodule
	
	