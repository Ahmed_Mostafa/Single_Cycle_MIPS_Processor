module tb_instr_mem;
	reg [31:0] mem_address;
	wire current_inst;
	instruction_memory test_instMem (current_inst,mem_address);
	initial begin
		#10 mem_address=0;
		#10 mem_address=1;
		#100 $finish;
	end
endmodule
