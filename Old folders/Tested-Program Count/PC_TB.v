module PC_test;
	reg[0:0] clk;
	reg [31:0] N;
	wire [31:0] P;
	PC salom(P,clk,N);
	initial		   
		begin
			clk<=0;
			N<=10;
			#5 clk=1;
			#1 N=15;
			#5 clk<=0;
			#5 clk<=1;
		end
endmodule
	