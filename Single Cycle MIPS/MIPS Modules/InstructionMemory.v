module InstructionMemory(current_inst,mem_address);
	input [31:0] mem_address;
	output[31:0] current_inst;
	wire [31:0] current_inst;
	reg [31:0]temp;
	reg [31:0] inst_memory [511:0]; 
	assign #200 current_inst=temp;
	initial begin
	  $readmemh("InstructionMemoryFile.h",inst_memory);  
	end
	
	always @(mem_address) begin
	  temp <= inst_memory[mem_address];
	end

endmodule
  
