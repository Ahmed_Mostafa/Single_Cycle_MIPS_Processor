module AluControl(op,Jr,ALUOP,funct);
	reg [3:0] temp;
	reg [0:0]temp_Jr;
	output [3:0]op; 
	output Jr;
	input [1:0]ALUOP;
	input [5:0]funct;
	//This module was assigned a delay of 50  unit
	assign #90 op=temp;
	assign #90 Jr=temp_Jr;
	always @(ALUOP,funct)
		begin
			temp_Jr=0;
			case(ALUOP) 
			2'b00:temp=4'b0010;//add
			2'b01:temp=4'b0110;//sub
			2'b10:
				case(funct)
				6'b000000:temp=4'b0011;//sll --Added by Bogy
				6'b001000:begin
					temp=4'b1111;//jr
					temp_Jr=1;
				end
				6'b100000:temp=4'b0010;//add
				6'b100010:temp=4'b0110; //sub
				6'b100100:temp=4'b0000;//and
				6'b100101:temp=4'b1100;//nor
				6'b101010:temp=4'b0111;//slt
				default:temp=4'bxxxx;
				endcase	
			2'b11:temp=4'b0000;//andi -- Added by Amr (Not in MIPS datapath)
			default:temp=4'bxxxx;
			endcase
		end
endmodule