module ControlUnit(Jump,SignedExtend,RegDst,ALUSrc,MemtoReg,RegWrite,MemRead,MemWrite,Branch,ALUOp,opCode);
	input [5:0] opCode;
	output Jump,SignedExtend,ALUSrc,RegWrite,MemRead,MemWrite,Branch;
	output [1:0] ALUOp,RegDst,MemtoReg;	//currently 2 bits will be changed to 4 when needed	  
	
	assign #90 Jump= (opCode == 6'b000000) ? 0: 
		(opCode == 6'b100011) ? 0:
		(opCode == 6'b101011) ? 0:
		(opCode == 6'b000100) ? 0:
		(opCode == 6'b001000) ? 0: //addi
		(opCode == 6'b001100) ? 0: //andi
		(opCode == 6'b000011) ? 1:0; //jal
	
	assign #90 SignedExtend=(opCode == 6'b000000) ? 1: 
		(opCode == 6'b100011) ? 1:
		(opCode == 6'b101011) ? 1:
		(opCode == 6'b000100) ? 1:
		(opCode == 6'b001000) ? 1: //addi
		(opCode == 6'b001100) ? 0: 1'bx;//andi
	
	assign #90 RegDst =  (opCode == 6'b000000) ? 2'b01: 
		(opCode == 6'b100011) ? 2'b00:
		(opCode == 6'b101011) ? 2'bxx:
		(opCode == 6'b000100) ? 2'bxx:
		(opCode == 6'b001000) ? 2'b00: //addi
		(opCode == 6'b001100) ? 2'b00: //andi
		(opCode == 6'b000011) ? 2'b10:2'bxx; //jal
	
	assign #90 ALUSrc =  (opCode == 6'b000000) ? 0: 
		(opCode == 6'b100011) ? 1:
		(opCode == 6'b101011) ? 1:
		(opCode == 6'b000100) ? 0:
		(opCode == 6'b001000) ? 1: //addi
		(opCode == 6'b001100) ? 1: 2'bx;//andi
	
	assign #90 MemtoReg =(opCode == 6'b000000) ? 2'b00: 
		(opCode == 6'b100011) ? 2'b01:
		(opCode == 6'b101011) ? 2'bxx:
		(opCode == 6'b000100) ? 2'bxx:
		(opCode == 6'b001000) ? 2'b00: //addi
		(opCode == 6'b001100) ? 2'b00: //andi
	    (opCode == 6'b000011) ? 2'b10: 2'bxx;	//jal
		
	assign #90 RegWrite =(opCode == 6'b000000) ? 1: 
		(opCode == 6'b100011) ? 1:
		(opCode == 6'b101011) ? 0:
		(opCode == 6'b000100) ? 0:
		(opCode == 6'b001000) ? 1: //addi
		(opCode == 6'b001100) ? 1: 0;//andi
	
	assign #90 MemRead =(opCode == 6'b000000) ? 0: 
		(opCode == 6'b100011) ? 1:
		(opCode == 6'b101011) ? 0:
		(opCode == 6'b000100) ? 0:
		(opCode == 6'b001000) ? 0: //addi
		(opCode == 6'b001100) ? 0: 0;//andi
	
	assign #90 MemWrite=(opCode == 6'b000000) ? 0: 
		(opCode == 6'b100011) ? 0:
		(opCode == 6'b101011) ? 1:
		(opCode == 6'b000100) ? 0:
		(opCode == 6'b001000) ? 0: //addi
		(opCode == 6'b001100) ? 0: 0;//andi
	
	assign #90 Branch = (opCode == 6'b000000) ? 0: 
		(opCode == 6'b100011) ? 0:
		(opCode == 6'b101011) ? 0:
		(opCode == 6'b000100) ? 1: 
		(opCode == 6'b001000) ? 0: //addi
		(opCode == 6'b001100) ? 0: 0;//andi
	
	assign #90 ALUOp =  (opCode == 6'b000000) ? 10: //r
		(opCode == 6'b100011) ? 00:	//load
		(opCode == 6'b101011) ? 00:	//store
		(opCode == 6'b000100) ? 01:	//bracnh			 
		(opCode == 6'b001000) ? 00: //addi
		(opCode == 6'b001100) ? 11: 2'bxx;//andi
	
	
endmodule