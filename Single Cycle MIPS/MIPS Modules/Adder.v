module Adder(OutAdder, A , B);
	input [31:0] A, B ;
	output [31:0]OutAdder ;
	reg [31:0] tmp;
	wire [31:0] OutAdder;
	always @( A , B)
	begin
		tmp <= A+B ;
	end	
	assign #70 OutAdder= tmp;
endmodule
