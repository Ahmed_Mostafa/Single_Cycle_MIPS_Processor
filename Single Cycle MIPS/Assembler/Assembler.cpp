#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <bitset>
#include <iomanip>

using namespace std;
#define FOR(i,n) for(int i=0; i<n; i++)

void trim_white_spaces(string& str)
{
	int first = str.find_first_not_of(' ');
	int last = str.find_last_not_of(' ');
	str= str.substr(first, (last - first + 1));
}

string to_binary_string(string str,int new_size)
{
	long long num;
	istringstream(str) >> num;
	string res = "";
	while (new_size--)
	{
		res += to_string(num & 1);
		num >>= 1;
	}
	reverse(res.begin(), res.end());	
	return res;
}

int main()
{	
	cout << "Enter the mips processor modules path :" << endl;
	string file_path; getline(cin, file_path);

	if (file_path[file_path.size() - 1] != '\\') {	file_path += "\\"; }

	string output_file = file_path + "InstructionMemoryFile.h";

	freopen("instructions_hex.h", "w", stdout);
	freopen("in.h", "r", stdin);

	vector<string> all_instructions; string input;
	map <string, int> leftLabels;
	while (getline(cin, input))
	{	
		all_instructions.push_back(input);		
	}

	int instructions_number = all_instructions.size();

	vector<vector<string>> semi_converted(instructions_number, vector<string>(8));

	FOR(i, instructions_number)
	{
		string instruction, operation, firstOperand, secondOperand, thirdOperand, rd, rs, rt, type, immediate, label, instruction_with_label_removed,shamt;
		int colonPos, firstSpaceOp1stOperandPos, lastSpaceOp1stOperandPos;

		instruction = all_instructions[i];

		istringstream ss(instruction);
		vector <string> comma_separated_tokens;
		string token;

		while (getline(ss, token, ','))
		{
			comma_separated_tokens.push_back(token);
		}

		FOR(j, comma_separated_tokens.size())
		{
			trim_white_spaces(comma_separated_tokens[j]);
		}

		colonPos = comma_separated_tokens[0].find(':');

		if (colonPos == -1)
		{
			firstSpaceOp1stOperandPos = comma_separated_tokens[0].find(' ');
			operation = comma_separated_tokens[0].substr(0, firstSpaceOp1stOperandPos);
			lastSpaceOp1stOperandPos = comma_separated_tokens[0].find_last_of(' ');
			firstOperand = comma_separated_tokens[0].substr(lastSpaceOp1stOperandPos + 1, comma_separated_tokens[0].length() - lastSpaceOp1stOperandPos-1);
		}
		else
		{
			label = comma_separated_tokens[0].substr(0, colonPos);
			trim_white_spaces(label);
			instruction_with_label_removed = comma_separated_tokens[0].substr(colonPos + 1, comma_separated_tokens[0].length()-colonPos);
			trim_white_spaces(instruction_with_label_removed);
			firstSpaceOp1stOperandPos = instruction_with_label_removed.find(' ');
			operation = instruction_with_label_removed.substr(0, firstSpaceOp1stOperandPos);
			lastSpaceOp1stOperandPos = instruction_with_label_removed.find_last_of(' ');
			firstOperand = instruction_with_label_removed.substr(lastSpaceOp1stOperandPos + 1,  instruction_with_label_removed.length() - lastSpaceOp1stOperandPos-1);
		}

		if (operation == "add" || operation == "sub" || operation == "and" || operation == "nor" || operation == "jr" || operation == "slt" || operation == "sll") // R format
		{
			if (operation != "jr")
			{
				secondOperand = comma_separated_tokens[1];
				thirdOperand = comma_separated_tokens[2];
				if (operation != "sll")
				{					
					rd = firstOperand; rs = secondOperand; rt = thirdOperand;
				}

				else
				{
					rd = firstOperand; rt = secondOperand; shamt = thirdOperand;  rs = "$0";
				}
			}

			else if (operation == "jr")
			{

				rs = firstOperand; rd = "$0"; rt = "$0";
			}

			type = "r";

		}

		else if (operation == "addi" || operation == "ori" || operation == "lw" || operation == "sw" || operation == "beq" || operation == "andi") // I format
		{
			if (operation == "addi" || operation == "ori" || operation == "andi" || operation == "beq")
			{

				secondOperand = comma_separated_tokens[1];
				thirdOperand = comma_separated_tokens[2];
				if (operation != "beq")
				{
					rt = firstOperand; rs = secondOperand; immediate = thirdOperand;
				}
				else if (operation == "beq")
				{
					rs = firstOperand; rt = secondOperand; immediate = thirdOperand;					
				}
			}

			else if (operation == "lw" || operation == "sw")
			{
				int first = comma_separated_tokens[1].find('(');
				int last = comma_separated_tokens[1].find(')');
				immediate = comma_separated_tokens[1].substr(0, first);
				secondOperand = comma_separated_tokens[1].substr(first + 1, last - 1 - first);
				rt = firstOperand; rs = secondOperand;
			}

			type = "i";
		}

		else if (operation == "jal")
		{
			immediate = firstOperand;
			type = "j";
		}
		
		if (colonPos != -1)
		{
			leftLabels[label] = i;
		}	
		
		if (type == "r")                                                      
		{		
			rs.erase(0, 1); rt.erase(0, 1);	rd.erase(0, 1);       
		}

		else if (type == "i")
		{
			rs.erase(0, 1); rt.erase(0, 1);			
		}		

		semi_converted[i][0] = type;
		semi_converted[i][1] = rs;
		semi_converted[i][2] = rt;
		semi_converted[i][3] = rd;
		semi_converted[i][4] = immediate;
		semi_converted[i][5] = label;
		semi_converted[i][6] = operation;
		semi_converted[i][7] = shamt;
	}

	vector<string> converted (instructions_number);
	vector<string> converted_hex(instructions_number);

	FOR(i, instructions_number)
	{
		if (semi_converted[i][0] == "r")
		{
			converted[i] += "000000";
			converted[i] += to_binary_string(semi_converted[i][1], 5) + to_binary_string(semi_converted[i][2], 5) + to_binary_string(semi_converted[i][3], 5);

			if (semi_converted[i][6] == "add")
			{
				converted[i] += "00000";
				converted[i] += "100000";

			}
			else if (semi_converted[i][6] == "and")
			{
				converted[i] += "00000";
				converted[i] += "100100";
			}
			else if (semi_converted[i][6] == "nor")
			{
				converted[i] += "00000";
				converted[i] += "100101";
			}

			else if (semi_converted[i][6] == "slt")
			{
				converted[i] += "00000";
				converted[i] += "101010";
			}
			else if (semi_converted[i][6] == "sll")
			{
				converted[i] += to_binary_string(semi_converted[i][7], 5);
				converted[i] += "000000";
			}
			else if (semi_converted[i][6] == "jr")
			{
				converted[i] += "00000";
				converted[i] += "001000";
			}
		}

		else if (semi_converted[i][0] == "i")
		{

			if (semi_converted[i][6] == "addi")
			{
				converted[i] += "001000";
				converted[i] += to_binary_string(semi_converted[i][1], 5) + to_binary_string(semi_converted[i][2], 5);
				converted[i] += to_binary_string(semi_converted[i][4], 16);
			}

			else if (semi_converted[i][6] == "andi")
			{
				converted[i] += "001100";
				converted[i] += to_binary_string(semi_converted[i][1], 5) + to_binary_string(semi_converted[i][2], 5);
				converted[i] += to_binary_string(semi_converted[i][4], 16);

			}

			else if (semi_converted[i][6] == "ori")
			{
				converted[i] += "001101";
				converted[i] += to_binary_string(semi_converted[i][1], 5) + to_binary_string(semi_converted[i][2], 5);
				converted[i] += to_binary_string(semi_converted[i][4], 16);

			}

			else if (semi_converted[i][6] == "beq")
			{
				converted[i] += "000100";
				converted[i] += to_binary_string(semi_converted[i][1], 5) + to_binary_string(semi_converted[i][2], 5);
				converted[i] += to_binary_string(to_string((leftLabels[semi_converted[i][4]]-i-1)),16);

			}

			else if (semi_converted[i][6] == "lw")
			{
				converted[i] += "100011";
				converted[i] += to_binary_string(semi_converted[i][1], 5) + to_binary_string(semi_converted[i][2], 5);
				converted[i] += to_binary_string(semi_converted[i][4], 16);


			}

			else if (semi_converted[i][6] == "sw")
			{
				converted[i] += "101011";
				converted[i] += to_binary_string(semi_converted[i][1], 5) + to_binary_string(semi_converted[i][2], 5);
				converted[i] += to_binary_string(semi_converted[i][4], 16);
			}

		}

		else if (semi_converted[i][0] == "j")
		{
			converted[i] += "000011";
			converted[i] +=  to_binary_string(to_string((leftLabels[semi_converted[i][4]])), 26);
		}
	}		

	FOR(i, instructions_number)
	{
		bitset <32> bset(converted[i]);
		cout << setfill('0') << setw(8)<< hex << bset.to_ullong() << endl;
	}
	
	
	fclose (stdout);
	int counter = 4;
	freopen(output_file.c_str(), "w", stdout);

	FOR(i, instructions_number)
	{
		bitset <32> bset(converted[i]);
		cout << setfill('0') << setw(8) << hex << bset.to_ullong() << endl;
		if(i != instructions_number-1) cout << "@" << hex << counter << endl;
		counter += 4;
	}

	
	return 0;
}